# Cavern Quest

This is a re-make of the very old TRS-80 game Cavern Quest.
I've even kept the very blocky monochrome graphics!

![screenshot](screenshot.png)

Game Play
---------

You have landed on an alien world, and need to refuel.
Alas, there are deadly creatures lurking about.
So avoid them at all costs! You can kill them, by dropping rocks on their heads.

Digging through the soil requires explosives, and you have a limited supply.
There are extra explosives that you can collect.

Once you have collected the fuel, head back to your ship. Bon voyage.

To Play
-------

To play the game, you must first install
[Tickle](https://nickthecoder.co.uk/software/tickle).

Start Tickle, then click "Play", and choose the file : "CavernQuest.tickle".
(If you've set up the proper file associations for ".tickle" files, you can double click it instead).


Missing Features
----------------

Enemies do not respawn when killed (still?)

Possible Improvements
---------------------

Have an "Options" scene :
    Choose the speed of the Player/Enemy
    Turn Respawn on/off
    Reduce the amount of "blasts" in each stash, so you have to be more efficient.

Respawned Enemies move faster

Powered by [Tickle](https://nickthecoder.co.uk/software/tickle) and [LWJGL](https://www.lwjgl.org/).
